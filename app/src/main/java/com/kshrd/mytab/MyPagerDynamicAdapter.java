package com.kshrd.mytab;

import android.content.Context;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pirang on 6/5/17.
 */

public class MyPagerDynamicAdapter extends FragmentPagerAdapter {

    List<Tab> tabList;
    Context context;

    public MyPagerDynamicAdapter(FragmentManager fm, Context context) {
        super(fm);
        tabList = new ArrayList<>();
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {
        return tabList.get(position).getFragment();
    }

    @Override
    public int getCount() {
        return tabList.size();
    }

    public void addTab(Tab tab) {
        tabList.add(tab);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        //return tabList.get(position).getTitle();
        return null;
    }

    public void tabLayoutIcon(TabLayout tabLayout) {
        for (int i = 0; i < tabList.size(); i++) {
            tabLayout.getTabAt(i).setIcon(tabList.get(i).getIcon());
        }
    }

    public void changeTablayoutCustomIcon(TabLayout tabLayout) {
        for (int i = 0; i < tabList.size(); i++) {
            tabLayout.getTabAt(i).setCustomView(getCustomView(i));
        }
    }

    private View getCustomView(int position) {
        Tab tab = tabList.get(position);
        View v = LayoutInflater.from(context).inflate(R.layout.custom_tab, null);
        TextView tvTitle = (TextView) v.findViewById(R.id.tvTitle);
        ImageView ivIcon = (ImageView) v.findViewById(R.id.ivIcon);
        tvTitle.setText(tab.getTitle());
        ivIcon.setImageResource(tab.getIcon());
        return v;
    }


}
