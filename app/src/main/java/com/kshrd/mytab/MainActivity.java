package com.kshrd.mytab;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    TabLayout tabLayout;
    ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        viewPager = (ViewPager) findViewById(R.id.viewPager);


//        MyPagerAdapter adapter = new MyPagerAdapter(getSupportFragmentManager());
//        adapter.addFragment(HomeFragment.newInstance("Home"));
//        adapter.addFragment(FavoriteFragment.newInstance("Favorite"));
//        viewPager.setAdapter(adapter);
//
//        tabLayout.setupWithViewPager(viewPager);

        MyPagerDynamicAdapter adapter = new MyPagerDynamicAdapter(getSupportFragmentManager(), this);
        adapter.addTab(new Tab("Home", R.drawable.selector_ic_home, HomeFragment.newInstance("Home")));
        adapter.addTab(new Tab("Favorite", R.drawable.selector_ic_favorite, FavoriteFragment.newInstance("Favorite")));
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
        //adapter.tabLayoutIcon(tabLayout);
        adapter.changeTablayoutCustomIcon(tabLayout);

    }
}
